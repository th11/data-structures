require_relative "static_array"

class DynamicArray
  attr_reader :length

  def initialize
    @store = StaticArray.new(8)
    @capacity = 8
    @length = 0
  end

  def [](index)
    check_index(index)
    store[index]
  end

  def []=(index, value)
    check_index(index)
    store[index] = value
  end

  def push(value)
    resize! if length == capacity

    # adding first so it passes check_index for []=
    self.length += 1
    self[length - 1] = value

    nil
  end

  def pop
    raise "can't pop when array is empty" unless (length > 0)

    value = self[length - 1]
    self[length - 1] = nil
    self.length -= 1

    value
  end

  def shift
    raise "can't shift when array is empty" unless (length > 0)
    value = self[0]

    (1...length).each { |i| self[i - 1] = self[i] }

    self.length -= 1

    value
  end

  def unshift(value)
    resize! if length == capacity

    # adding first so it passes check_index for []=
    self.length += 1

    (length - 2).downto(0) { |i| self[i + 1] = self[i] }

    self[0] = value

    nil
  end

  def resize!
    new_capacity = capacity * 2
    new_store = Array.new(new_capacity)

    length.times { |i| new_store[i] = self[i] }

    self.capacity = new_capacity
    self.store = new_store
  end

  protected
  attr_accessor :store, :capacity
  attr_writer :length

  def check_index(index)
    unless (index >= 0) && (index < length)
      raise "invalid index"
    end
  end
end

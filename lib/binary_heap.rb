require 'byebug'

class Entry
  attr_accessor :key, :value

  def initialize(key = nil, value = nil)
    @key, @value = key, value
  end
end

class BinaryHeap
  #  use array method. start at index 1

  def initialize
    @store = []
    @last_index = 0
    @count = 0
  end

  def insert(key, value)
    # add in last place and heapify up

    raise ArgumentError.new("Key must be numeric!") unless key.is_a?(Numeric)

    self.last_index += 1

    store[last_index] = Entry.new(key, value)

    current_index = last_index
    parent_index = current_index / 2

    while parent_index >= 1 &&
      store[parent_index].key > store[current_index].key

      store[parent_index], store[current_index] =
        store[current_index], store[parent_index]

      current_index = parent_index
      parent_index = current_index / 2
    end

    self.count += 1

    nil
  end

  def remove
    # remove root, replace root with last item and heapify down
    # children 2i & 2i + 1

    raise "Heap is empty" if count == 0

    prc ||= Proc.new { |i1, i2| i1.key <=> i2.key }

    old_root = store[1]
    store[1], store[last_index] = store[last_index], nil
    self.last_index -= 1
    self.class.heapify_down(store, 1, last_index, &prc)

    self.count -= 1

    old_root.value
  end

  def top # shows root node
    raise "Heap is empty" if count == 0

    store[1].value
  end

  def self.child_indices(parent_index, last_index)
    [parent_index * 2, (parent_index * 2) + 1].select do |i|
      i <= last_index
    end
  end

  def self.heapify_down(arr, parent_idx, last_idx, &prc)
    prc ||= Proc.new { |i1, i2| i1.key <=> i2.key }

    child1_idx, child2_idx = self.child_indices(parent_idx, last_idx)

    children = []
    children << child1_idx if child1_idx
    children << child2_idx if child2_idx

    if children.all? { |idx| prc.call(arr[parent_idx], arr[idx]) <= 0 }
      return arr
    end

    switch_index = nil

    if children.length == 1
      switch_index = child1_idx
    else
      switch_index = prc.call(arr[child1_idx], arr[child2_idx]) <= 0 ?
        child1_idx : child2_idx
    end

    arr[parent_idx], arr[switch_index] = arr[switch_index], arr[parent_idx]

    self.class.heapify_down(arr, switch_index, last_idx, &prc)
  end

  protected

  attr_accessor :store, :last_index, :count
end

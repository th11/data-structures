require_relative 'node'

class Queue
  attr_reader :length

  def initialize
    @first, @last, @length = nil, nil, 0
  end

  def enqueue(value)
    old_last = last
    self.last = Node.new(value, nil)

    if empty?
      self.first = last
    else
      old_last.next = last
    end

    self.length += 1

    nil
  end

  def dequeue
    raise "Can't dequeue from empty list" if empty?

    old_first = first
    self.first = first.next
    self.last = nil if empty?

    self.length -= 1

    old_first.value
  end

  def empty?
    first.nil?
  end

  protected

  attr_accessor :first, :last
  attr_writer :length
end

require_relative 'ring_buffer'

class Stack
  def initialize
    @store = RingBuffer.new
  end

  def push
    store.push
  end

  def pop
    store.pop
  end

  private
  
  attr_reader :store
end

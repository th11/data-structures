require "singly_linked_list"

describe SList do
  before do
    @list = SList.new
  end

  it "initializes correctly" do
    expect(@list.size).to be(0)
    expect(@list.head).to be(nil)
  end

  it "inserts value at front of list" do
    @list.insert_front(1)
    @list.insert_front(2)
    expect(@list.head.value).to be(2)
    expect(@list.size).to be(2)
  end

  it "inserts value at end of list" do
    @list.insert_end(1)
    expect(@list.head.value).to be(1)
    @list.insert_end(2)
    expect(@list.head.next.value).to be(2)
    @list.insert_end(3)
    expect(@list.head.next.next.value).to be(3)
    expect(@list.size).to be(3)
  end

  it "returns value at specified index" do
    @list.insert_front(1)
    @list.insert_end(2)
    @list.insert_end(3)

    expect(@list[0]).to be(1)
    expect(@list[1]).to be(2)
    expect(@list[2]).to be(3)
    expect{ @list[3] }.to raise_error
  end

  it "has remove operation" do
    @list.insert_front(1)
    @list.insert_end(2)
    @list.remove(1)
    expect(@list.head.value).to be(2)
    @list.insert_end(3)
    @list.remove(3)
    expect(@list.head.value).to be(2)
  end

  it "has remove_all operation" do
    2.times { @list.insert_front(1) }
    2.times { @list.insert_front(2) }
    expect(@list.size).to be(4)
    @list.remove_all(1)
    expect(@list.size).to be(2)
  end
end

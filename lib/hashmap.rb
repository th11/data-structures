class Entry
  attr_accessor :key, :value

  def initialize(key = nil, value = nil)
    @key, @value = key, value
  end
end


class Hashmap
  INITIAL_SIZE = 8
  LOAD_FACTOR = 1

  def initialize
    @buckets = Array.new(INITIAL_SIZE) { [] }
    @count = 0
  end

  def bucket_for(key, buckets = self.buckets)
    key.hash % buckets.length
  end

  def delete(key)
    bucket = buckets[bucket_for(key)]
    prc = Proc.new { |entry| entry.key == key }

    entry = bucket.find(&prc)

    return unless entry

    bucket.delete_if(&prc)
    self.count -= 1

    entry.value
  end

  def [](key)
    bucket = buckets[bucket_for(key)]

    return nil if bucket.empty?

    bucket.each do |entry|
      return entry.value if entry.key == key
    end

    nil
  end

  def []=(key, value)
    resize! if count.fdiv(buckets.length) > LOAD_FACTOR

    bucket = buckets[bucket_for(key)]

    entry = bucket.find { |entry| entry.key == key }

    if entry.nil?
      bucket << Entry.new(key, value)
      self.count += 1
    else
      entry.value = value
    end

    nil
  end

  def resize!
    new_size = buckets.length * 2
    new_buckets = Array.new(new_size) { [] }

    buckets.each do |bucket|
      bucket.each do |entry|
        new_buckets[bucket_for(entry.key, new_buckets)] << entry
      end
    end

    self.buckets = new_buckets
  end

  protected

  attr_accessor :buckets, :count
end
